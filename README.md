# Die vielen Seiten von Python: das ABC des Programmierens

Mit diesen Python-Beispielen unterrichte ich Programmieren; gedacht ist der 
Kurs für Anfänger:innen ab etwa einem Alter von 12 Jahren (also Schulstufe 6).

Ich verwende den Mu-Editor: <https://codewith.mu/>

Es gibt zwei Arten von Beispielen, für die man jeweils den entsprechende Modus
in Mu einstellen muss:

+ Python 3: Programme mit Ein- und Ausgabe von Text
+ Pygame Zero: Bilder, Animationen und Spiele

Der Kurs beginnt mit `am_anfang_hallo`; dann geht es weiter, Buchstabe für 
Buchstabe: die Namen der Beispiele sind alphabetisch geordnet.

Als Motto dieses Kurses wähle ich den Leitspruch, der das Design von 
Scratch beeinflußt hat:

> Low floor, wide walls, high ceiling
>
> <https://gordonbrander.com/pattern/low-floor-wide-walls-high-ceiling/>
