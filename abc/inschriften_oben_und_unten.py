# Spiel mit Darstellung von Text
# Dokumentation:
# https://pygame-zero.readthedocs.io/en/stable/ptext.html
# https://codewith.mu/en/howto/1.2/pgzero_sounds_images
# Copyright 2023 Roland Richter                        [Mu: Pygame Zero]

# Erzeuge ein rechteckiges 900×600 Fenster
WIDTH = 900
HEIGHT = 600

botschaft = ""

hgfarbe = "#1C39BB"   # Persian blue
textfarbe = "yellow"

transparenz = 1.0
winkel = 0

def draw():
    # Setze den Hintergrund auf "Persian blue"
    screen.fill(hgfarbe)

    # Begrüße alle in der Mitte der Leinwand
    screen.draw.text("Hallo Welt!", center=(WIDTH / 2, 100),
                     color=textfarbe,
                     fontname="rujis_handwriting", fontsize=42)

    # Ruji's Handwriting Font v.2.0, OFL (SIL Open Font License) Lizenz
    # https://fontlibrary.org/en/font/ruji-s-handwriting-font-v-2-0
    # ACHTUNG: der Name der Schriftart muss auf kleinbuchstaben geändert werden
    screen.draw.text(botschaft, center=(WIDTH / 2, 200),
                     color=textfarbe,
                     fontname="rujis_handwriting", fontsize=42)

    screen.draw.text(botschaft, center=(WIDTH / 2, 300),
                     color=textfarbe,
                     fontname="rujis_handwriting", fontsize=42,
                     shadow=(2,2), scolor="#202020")

    screen.draw.text(botschaft, center=(WIDTH / 2, 400),
                     color=textfarbe, alpha=transparenz,
                     fontname="rujis_handwriting", fontsize=42,
                     angle=winkel)


def update():
    global transparenz, winkel
    if transparenz > 0.0:
        transparenz *= 0.999
    winkel += 1


def on_key_down(key, mod, unicode):
    print(key)
    global botschaft, hgfarbe, textfarbe, transparenz, winkel
    if key == keys.RETURN:
        botschaft = ""
        textfarbe, hgfarbe = hgfarbe, textfarbe   # vertausche beide Farben
        transparenz = 1.0
        winkel = 0
    elif key == keys.BACKSPACE:
        if len(botschaft) > 0:
            botschaft = botschaft[:-1]
    else:
        botschaft += unicode


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
