# Zeige einige Informationen über die Programmiersprache Python
# Copyright 2021-2023 Roland Richter                     [Mu: Python 3]

# Schreibe Text in die Konsole
print("Hallo Welt!")

# QUIZ Jede dieser Zeilen ist fehlerhaft; siehst du, warum?
#      Lösche ein '#'-Zeichen und beobachte, was passiert:
# pirnt("Hallo Welt!")
# print("Hallo Welt!"
# print("Hallo Welt!)
# print("Hallo Welt!"))
# print("Hallo Wlet!")

# TIPP Um deinen Code auf Fehler zu prüfen, drücke "Prüfen";
#      oft ist es auch hilfreich, deinen Code mit "Tidy" aufzuräumen.

# Noch einige Fakten über Python:
name = "Guido van Rossum"
year = 1991
# `name` ist eine Variable und enthält Text; `year` ist auch eine Variable
# und enthält eine Zahl. print() kann sowohl Text als auch Zahlen ausgeben.
print("Python wurde", year, "von", name, "entwickelt.")

# TIPP Du kannst sowohl "" als auch '' als Anführungszeichen für Text verwenden
#     🠗                🠗                             🠗                🠗
print('2021 war Python "Programmiersprache des Jahres" des TIOBE Index')

# TIPP Das "Schlange"-Zeichen findest du auf mehreren Seiten, z.B.
# https://symbl.cc/en/1F40D-snake-emoji/ und https://emojipedia.org/snake
print("Für Python wird oft das 'Schlange'-Zeichen verwendet", "🐍")

# Mit + kannst du nicht nur Zahlen addieren, sondern auch Text verbinden:
flying_circus = "Monty Python"
print("aber der Name stammt von der britischen Komikergruppe " + flying_circus)
print("Die aktuelle Version der Sprache ist Python", 1 + 1 + 1)


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
