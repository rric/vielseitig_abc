# Lerne einiges über Koordinaten, Farbnamen und Farbcodes
# Copyright 2023-2024 Roland Richter                   [Mu: Pygame Zero]

# Erzeuge ein rechteckiges Fenster der Größe 800×500
WIDTH = 800
HEIGHT = 500

# Eine Position im Fenster kannst du mit (x, y)-Koordinaten angeben:
# x ... horizontaler Wert, von links nach rechts (wie in Mathe)
# y ... vertikaler Wert, von oben nach unten (anders als in Mathe!)
# In Python beginnt man _nicht_ mit 1, sondern mit 0 zu zählen:
# x ... Werte von 0 bis WIDTH-1
# y ... Werte von 0 bis HEIGHT-1
# Position (0, 0) ist also die linke obere Ecke des Fensters; etc.
# ÄNDERE den Code so, dass die Eckpunkte die korrekten Werte haben.
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
links_oben = (1, 1)
rechts_oben = (WIDTH - 1, 1)
links_unten = (WIDTH - 1, HEIGHT - 1)
rechts_unten = (1, HEIGHT + 1)
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

# In Python nennt man einen Ausdruck wie (800, 500) auch ein "Tuple".
# ÄNDERE den Code so, dass 'mitte' die Mitte des Fensters ist.
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
mitte = (800, 500)
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

# Farben kannst du mit Namen wie "white", "yellow" etc. angeben,
# siehe https://www.pygame.org/docs/ref/color_list.html
# ÄNDERE den Code so, dass alle Farbnamen richtig sind.
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
schwarz = "blakc"
orange = "ORANGE"
silber = "silber"
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

# Wenn du Farben brauchst, die auf der Pygame-Seite nicht angeführt sind,
# kannst du einen Hex-Code verwenden, z.B. von https://latexcolor.com/
# ÄNDERE den Code so, dass die Farben den richtigen Hex-Code haben.
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
mikado_yellow = "FFC40C"
ocean_boat_blue = "#07B"
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

def draw():
    try:
        screen.fill(ocean_boat_blue)
        screen.draw.text(
            "Du hast es geschafft! Bravo!", center=(400, 200), color=mikado_yellow, fontsize=36
        )
        screen.draw.text(
            "Schreibe jetzt ein bisschen was zu dir ...", center=mitte, color=orange, fontsize=42
        )
    except:
        pass


# HALT! Änderungen unter dieser Zeile sind verboten!
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

frame_count = 0

def fail_string(name, val, exp):
    return "  " + name + " ist " + str(val) + ", sollte aber " + str(exp) + " sein"


def update():
    # update() wird vor draw() aufgerufen; prüfe asserts erst *nach* dem ersten draw()
    global frame_count, color_names
    frame_count += 1

    if frame_count == 1:
        return

    sf_size = screen.surface.get_size()
    assert sf_size == (800, 500)

    if not (links_oben == (0, 0)):
        print(fail_string("links_oben", links_oben, (0, 0)))

    if not (rechts_oben == (WIDTH - 1, 0)):
        print(fail_string("rechts_oben", rechts_oben, (WIDTH - 1, 0)))

    if not (links_unten == (0, HEIGHT - 1)):
        print(fail_string("links_unten", links_unten, (0, HEIGHT - 1)))

    if not (rechts_unten == (WIDTH - 1, HEIGHT - 1)):
        print(fail_string("rechts_unten", rechts_unten, (WIDTH - 1, HEIGHT - 1)))

    if not (mitte == (WIDTH // 2, HEIGHT // 2)):
        print(fail_string("mitte", mitte, (WIDTH // 2, HEIGHT // 2)))

    if not schwarz == "black":
        print(fail_string("schwarz", schwarz, "black"))

    if not orange == "orange":
        print(fail_string("orange", orange, "orange"))

    if not silber == "silver":
        print(fail_string("silber", silber, "silver"))

    if not mikado_yellow == "#FFC40C":
        print(fail_string("mikado_yellow", mikado_yellow, "#FFC40C"))

    if not ocean_boat_blue == "#0077BE":
        print(fail_string("ocean_boat_blue", ocean_boat_blue, "#0077BE"))


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
