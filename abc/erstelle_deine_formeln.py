# Zeigt Berechnungen mit ganzen Zahlen und Kommazahlen
# Copyright 2022 Roland Richter                           [Mu: Python 3]

print("=== Arithmetische Operatoren ===")

print("Addition: 42 + 3.14 ist", 42 + 3.14)
print("Subtraktion: 42 - 3.14 ist", 42 - 3.14)
print("Multiplikation: 42 * 3.14 ist", 42 * 3.14)

print("Division: 17 / 5 ist", 17 / 5)
print("Ganzzahlige Division: 17 // 5 ist", 17 // 5)
print("Rest bei Division (modulo): 17 % 5 ist", 17 % 5)

print("=== Zuweisungsoperatoren ===")

x = 42
print("mache 'x = 42', und x ist", x)
x += 3.14
print("mache 'x += 3.14', und x ist jetzt", x)

y = 42
print("mache 'y = 42', und y ist", y)
y -= 3.14
print("mache 'y -= 3.14', und y ist jetzt", y)

z = 42
print("mache 'z = 42', und z ist", z)
z *= 3.14
print("mache 'z *= 3.14', und z ist jetzt", z)

print("=== Vergleichsoperatoren ===")

if x == y:
    print("x ist gleich y:", x, "==", y)
if x != y:
    print("x ist ungleich y:", x, "!=", y)
if x < y:
    print("x ist kleiner als y:", x, "<", y)
if x > y:
    print("x ist größer als y:", x, ">", y)
if y <= z:
    print("y ist kleiner oder gleich z:", y, "<=", z)
if y >= z:
    print("y ist größer oder gleich z:", y, ">=", z)

print("=== Ganze Zahlen (integers) ===")

print("Die Antwort ist:", 42)
print("Die Zahl 42 als binäre Zahl:", bin(42))  # starts with prefix 0b
print("Die Zahl 42 als hexadezimale Zahl:", hex(42))  # starts with prefix 0x

print(
    "Größte natürliche 8-bit Zahl:",
    bin(0b11111111),
    "=",
    0b11111111,
    "=",
    hex(0b11111111),
)

# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
