# Sonnenuntergang in Anlehnung an "Carla's Island", eine bahnbrechende
# Computeranimation von Nelson Max aus dem Jahr 1981, siehe z.B.
# https://archive.org/details/CarlasIsland
# Copyright 2021--2023 Roland Richter                  [Mu: Pygame Zero]

# Erzeuge ein 800×500 Fenster mit Titel
TITLE = "Grafik bis zum Abendrot"
WIDTH = 800
HEIGHT = 500

frame_count = 0
total_time = 0.0
delta_times = []

sun_x = WIDTH
sun_y = 30

light_sky_blue = (135, 206, 235)
warm_black = (0, 66, 66)

yellow = (255, 255, 0)
darkorange2 = (238, 118, 0)


def lerp_color(beg_clr, end_clr, factor):
    return (
        round((1.0 - factor) * beg_clr[0] + factor * end_clr[0]),
        round((1.0 - factor) * beg_clr[1] + factor * end_clr[1]),
        round((1.0 - factor) * beg_clr[2] + factor * end_clr[2]),
    )


def draw():
    # Zähle die Anzahl der gezeichneten frames mit
    global total_time, frame_count
    frame_count += 1

    # Setze den Hintergrund auf eine "interpolierte Farbe"
    if frame_count < 200:
        factor = 0.0
    elif frame_count < 400:
        factor = (frame_count - 200) / 200.0
    else:
        factor = 1.0

    bg_color = lerp_color(light_sky_blue, warm_black, factor)
    screen.fill(bg_color)

    # Male das Meer im unteren Teil des Fensters, dahinter eine Insel
    screen.draw.filled_circle((0.5 * WIDTH, HEIGHT), 0.3 * HEIGHT, "forestgreen")
    screen.draw.filled_rect(Rect(0, 0.8 * HEIGHT, WIDTH, 0.2 * HEIGHT), "royalblue4")

    # Male eine weiße Wolke ...
    screen.draw.filled_circle((600, 150), 42, "white")
    screen.draw.filled_circle((576, 155), 42, "white")
    screen.draw.filled_circle((616, 140), 42, "white")
    screen.draw.filled_circle((632, 160), 42, "white")

    # Male einen Leuchtturm ...
    screen.draw.filled_rect(Rect(0.4 * WIDTH, 0.6 * HEIGHT, 20, 80), "whitesmoke")

    # ... mit einem Blinklicht, das zwischen rot und schwarz hin- und herspringt.
    if frame_count % 120 < 20:
        screen.draw.filled_circle((0.4 * WIDTH + 10, 0.6 * HEIGHT), 10, "red")
    else:
        screen.draw.filled_circle((0.4 * WIDTH + 10, 0.6 * HEIGHT), 10, "black")

    # Schreibe Informationen in die linke ...
    avg_delta = sum(delta_times) / len(delta_times)
    frame_rate = 1.0 / avg_delta
    info = "frame " + str(frame_count) + " (" + "{:.2f}".format(frame_rate) + " fps)"
    screen.draw.text(info, (10, 10), color="white", fontsize=24)

    # ... und in die rechte obere Ecke
    info = "time: " + "{:.2f}".format(total_time) + " s"
    screen.draw.text(info, (WIDTH - 100, 10), color="white", fontsize=24)

    # Male die Sonne
    sun_color = lerp_color(yellow, darkorange2, factor)
    screen.draw.filled_circle((sun_x, sun_y), 65, sun_color)

    # Hoppla! Die Sonne sollte doch hinter dem Meer und der Wolke verschwinden.
    # PROBIERE, die Sonne so zu zeichnen, dass sie hinter dem Meer und hinter
    # der Wolke verschwindet.


def update(delta_time):
    # delta_time ist die Zeit in Sekunden, die seit dem letzten Aufruf vergangen sind:
    # https://pygame-zero.readthedocs.io/en/stable/hooks.html?highlight=update()#update
    global total_time, delta_times, sun_x, sun_y
    total_time += delta_time

    delta_times.append(delta_time)
    if len(delta_times) > 10:
        delta_times.pop(0)

    # Bewege die Sonne nach links unten
    sun_x -= 2
    sun_y += 1


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
