# Ein UFO im Anflug auf unseren Meeresblick ...
# Copyright (C) 2021, 2023  Roland Richter             [Mu: Pygame Zero]

# Erzeuge ein 800×500 Fenster mit Titel
TITLE = "Flugobjekt von links ..."
WIDTH = 800
HEIGHT = 500
frame_count = 0

ufo_x = 0
ufo_y = 170


def draw():
    # Zähle die Anzahl der gezeichneten frames mit
    global frame_count
    frame_count += 1

    screen.fill("skyblue2")

    # Male das Meer im unteren Teil des Fensters, dahinter eine Insel
    screen.draw.filled_circle((0.5 * WIDTH, HEIGHT), 0.3 * HEIGHT, "forestgreen")
    screen.draw.filled_rect(Rect(0, 0.8 * HEIGHT, WIDTH, 0.2 * HEIGHT), "royalblue4")

    # Male eine weiße Wolke ...
    screen.draw.filled_circle((600, 150), 42, "white")
    screen.draw.filled_circle((576, 155), 42, "white")
    screen.draw.filled_circle((616, 140), 42, "white")
    screen.draw.filled_circle((632, 160), 42, "white")

    # ... und die gelbe Sonne
    screen.draw.filled_circle((150, 70), 45, "yellow")

    # PROBIERE, auch die Sonne zu bewegen!

    # Male einen Leuchtturm ...
    screen.draw.filled_rect(Rect(0.4 * WIDTH, 0.6 * HEIGHT, 20, 80), "whitesmoke")

    # ... mit einem Blinklicht, das zwischen rot und schwarz hin- und herspringt.
    if frame_count % 120 < 20:
        screen.draw.filled_circle((0.4 * WIDTH + 10, 0.6 * HEIGHT), 10, "red")
    else:
        screen.draw.filled_circle((0.4 * WIDTH + 10, 0.6 * HEIGHT), 10, "black")

    # PROBIERE, das Blinklicht schneller oder langsamer zu machen!

    # Zeichne das UFO
    draw_ufo()


def update():
    global ufo_x, ufo_y

    # Bewege das UFO nach rechts; ...
    ufo_x += 2
    # falls rechter Rand erreicht, beginne wieder am linken Rand.
    if ufo_x > WIDTH:
        ufo_x = 0

    # PROBIERE, das Ufo von rechts nach links fliegen zu lassen!


def draw_ufo():
    # Male den Antrieb des UFOs (gelb-rote Ionenstrahlen), ...
    if frame_count % 4 == 0:
        screen.draw.filled_circle((ufo_x, ufo_y + 40), 15, "red")
        screen.draw.filled_circle((ufo_x, ufo_y + 40), 10, "yellow")

    # ... und das UFO selbst.
    screen.draw.filled_rect(Rect(ufo_x - 70, ufo_y - 10, 140, 30), "rosybrown4")
    screen.draw.filled_circle((ufo_x, ufo_y), 42, "rosybrown4")


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
