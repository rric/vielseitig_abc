# Lerne, wie man mit Kreisen und Rechtecken malen kann
# Copyright 2023-2024 Roland Richter                   [Mu: Pygame Zero]

# Erzeuge ein rechteckiges 800×500 Fenster
TITLE = "Dunkelblauer Meeresblick"
WIDTH = 800
HEIGHT = 500

# Die Funktion draw() wird von Pygame Zero aufgerufen, um das Fenster neu
# zu malen; siehe https://pygame-zero.readthedocs.io/en/stable/hooks.html
def draw():
    # Ich möchte den Himmel in "skyblue2" einfärben. Die Funktion fill()
    # malt das gesamte Fenster in einer Farbe aus -- perfekt! ... oder?
    # ÄNDERE den Code so, dass die richtige Farbe verwendet wird.
    # ↓---------↓---------↓---------↓---------↓---------↓---------↓
    screen.fill("steelblue2")
    # ↑---------↑---------↑---------↑---------↑---------↑---------↑

    # Male das Meer in "royalblue4" im unteren Teil des Fensters, und
    # male dahinter eine Insel in "forestgreen"
    # ÄNDERE den Code so, dass die Insel nicht mehr vor dem Meer liegt.
    # ↓---------↓---------↓---------↓---------↓---------↓---------↓
    screen.draw.filled_rect(Rect(0, 400, WIDTH, 100), "royalblue4")
    screen.draw.filled_circle((400, 500), 150, "forestgreen")
    # ↑---------↑---------↑---------↑---------↑---------↑---------↑

    global sun_pos
    # Male eine gelbe Sonne ... Hm, du kannst die Sonne nicht sehen?
    # ÄNDERE die Position so, dass eine gelbe Sonne sichtbar ist
    # ↓---------↓---------↓---------↓---------↓---------↓---------↓
    sun_pos = (850, 70)
    screen.draw.filled_circle(sun_pos, 55, "orange")
    # ↑---------↑---------↑---------↑---------↑---------↑---------↑

    # Male zum Schluß noch eine weiße Wolke ...
    screen.draw.filled_circle((0.75 * WIDTH, 0.30 * HEIGHT), 42, "white")
    screen.draw.filled_circle((0.72 * WIDTH, 0.31 * HEIGHT), 42, "white")
    screen.draw.filled_circle((0.77 * WIDTH, 0.28 * HEIGHT), 42, "white")
    screen.draw.filled_circle((0.79 * WIDTH, 0.32 * HEIGHT), 42, "white")


# HALT! Änderungen unter dieser Zeile sind verboten!
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

frame_count = 0

# color_names ist ein (sehr kleiner) Teil des umgekehrten
# https://github.com/pygame/pygame/blob/main/src_py/colordict.py
# Farbe (R, G, B) wird zu 65536*R + 256*G + B
color_names = {}

def rgb2int(r, g, b):
    return 65536 * r + 256 * g + b

def fail_string(name, val, exp):
    return "\n  " + name + " ist " + str(val) + ", sollte aber " + str(exp) + " sein"

def test_pixel(pos, xpct_int):
    pxl = screen.surface.get_at_mapped(pos)
    if not pxl == xpct_int:
        print(
            "  Pixel" + str(pos) + " ist " + color_names[pxl] + ",",
            "sollte aber " + color_names[xpct_int] + " sein",
        )

def update():
    # update() wird vor draw() aufgerufen; prüfe asserts erst *nach* dem ersten draw()
    global frame_count, color_names
    frame_count += 1

    if frame_count == 1:
        color_names[rgb2int(34, 139, 34)] = "forestgreen"
        color_names[rgb2int(126, 192, 238)] = "skyblue2"
        color_names[rgb2int(92, 172, 238)] = "steelblue2"
        color_names[rgb2int(39, 64, 139)] = "royalblue4"
        color_names[rgb2int(255, 165, 0)] = "orange"
        color_names[rgb2int(255, 255, 0)] = "yellow"
        color_names[rgb2int(255, 255, 255)] = "white"
        return

    sf_size = screen.surface.get_size()
    assert sf_size == (800, 500)

    if not (sun_pos[0] >= 55 and sun_pos[0] <= 745):
        print(fail_string("Der x-Wert von sun_pos", sun_pos[0], "zwischen 55 und 745"))
    if not (sun_pos[1] >= 55 and sun_pos[1] <= 345):
        print(fail_string("Der y-Wert von sun_pos", sun_pos[0], "zwischen 55 und 345"))

    test_pixel((400, 5), rgb2int(126, 192, 238))  # skyblue2
    test_pixel((400, 450), rgb2int(39, 64, 139))  # royalblue4
    test_pixel((400, 390), rgb2int(34, 139, 34))  # forestgreen

    if (sun_pos[0] >= 55 and sun_pos[0] <= 745) and (
        sun_pos[1] >= 55 and sun_pos[1] <= 345
    ):
        test_pixel(sun_pos, rgb2int(255, 255, 0))  # yellow


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
