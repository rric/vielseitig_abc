# Eine Menge an Bäumen ziehen vorbei ...
# Copyright 2023 Roland Richter                        [Mu: Pygame Zero]

import random

# Erzeuge ein rechteckiges 800×500 Fenster
TITLE = "Schwebende UFO-Sterne"
WIDTH = 800
HEIGHT = 500
frame_count = 0

horizon_y = 400

ufo = Actor("kenney_alien-ufo-pack/png/shippink_manned.png", (WIDTH//2, 140))
dome = Actor("kenney_alien-ufo-pack/png/dome.png", (WIDTH//2, 140 - 30))
star = None
ufo_speed = 1
ufo_lift = 0
star_descent = 0

# Erzeuge 125 Bäume:
# - horizontal alle 80 Pixel
# - vertikal zwischen 350 und 450
# Der Anchor-Punkt ist oben in der Mitte, wo der Stern befestigt werden soll.
wood = []
for num in range(125):
    tree = Actor(
        "kenney_platformer-art-winter/tiles/pinesapling.png",
        anchor=("center", "top"),
        midtop=(80 * num + WIDTH//2, random.randint(350, 450)),
    )
    wood.append(tree)

# Es gibt zwei Listen von Sternen:
# - die Sterne, die an einem Baum befestigt wurden
# - die Sterne, die hinunter gefallen und am Boden verstreut sind
attached = []
scattered = []

game_finished = False

# Holiday / Seasonal 4 by Diego Nava from https://mixkit.co/free-stock-music/tag/christmas/
music.play("mixkit-holiday-seasonal-4-511.mp3")
music.set_volume(0.4)

def draw():
    screen.fill("skyblue2")

    # Male Schnee in den unteren Teil des Fensters
    screen.draw.filled_rect(Rect(0, horizon_y, WIDTH, HEIGHT - horizon_y), "snow")

    # Male die Sonne
    screen.draw.filled_circle((150, 70), 45, "yellow")

    # Male zuerst die am Boden verstreuten Sterne, ...
    for scad in scattered:
        scad.draw()

    # ... dann die Bäume, und dann erst ...
    for tree in wood:
        tree.draw()

    # ... die an Bäumen befestigten Sterne.
    for attd in attached:
        attd.draw()

    if star_descent:
        star.draw()

    # Zeichne das UFO mit der Hülle
    ufo.draw()
    dome.draw()

    # PROBIERE Zeige in einer Ecke des Fensters Informationen an, z.B.
    # - wie viele Sterne an einer Baumspitze befestigt sind;
    # - wie viele Sterne das UFO insgesamt abgeworfen hat.
    # TIPP verwende die Länge der Listen 'attached' und 'scattered'

    global frame_count
    frame_count += 1


def update():
    global game_finished, wood, horizon_y, ufo_speed, ufo_lift, star, star_descent

    # Lass das Ufo wackeln; erzeuge dazu einen zufälligen Winkel
    winkel = random.randint(-8, 8)
    ufo.angle = winkel

    # Die Hülle des UFOs muss ebenfalls mitbewegt werden
    dome.x = ufo.x
    dome.y = ufo.y - 30
    dome.angle = ufo.angle

    if ufo_speed != 0:
        for tree in wood:
            tree.x -= ufo_speed
        for scad in scattered:
            scad.x -= ufo_speed
        for attd in attached:
            attd.x -= ufo_speed

    if ufo_lift != 0:
        horizon_y += ufo_lift
        for tree in wood:
            tree.y += ufo_lift
        for scad in scattered:
            scad.y += ufo_lift
        for attd in attached:
            attd.y += ufo_lift
        ufo_lift = 0

    if not star is None:
        star.y += star_descent
        star_descent += 0.2

        # Falls der Stern zu tief nach unten gefallen ist, ...
        if star.y >= horizon_y+90:
            # ... bleibt er verstreut am Boden liegen;
            star.angle = random.randint(45, 135)
            scattered.append(star)
            star = None
            star_descent = 0
            sounds.impactglass_light_003.play()
        # falls der Stern noch hinunter fällt, ...
        else:
            # ... wird für jede Baumspitze die Distanz berechnet;
            for tree in wood:
                # falls der Stern ganz nahe zu einer Baumspitze ist, ...
                if tree.distance_to(star) <= 9.0:
                    # ... wird er jetzt an diesem Baum befestigt.
                    star.center = tree.midtop
                    attached.append(star)
                    star = None
                    star_descent = 0
                    sounds.impactbell_heavy_000.play()
                    break

    # Wenn der letzte Baum erreicht wird, ist das Spiel zu Ende!
    if wood[-1].x <= WIDTH//2:
        game_finished = True
        ufo_speed = -3


# Verändere Geschwindigkeit und Auftrieb mit ⭠, ⭢ und ⭡; siehe auch
# https://pygame-zero.readthedocs.io/en/stable/hooks.html#buttons-and-keys
def on_key_down(key):
    global game_finished, wood, horizon_y, ufo_speed, ufo_lift, star, star_descent

    if game_finished:
        return

    if key == keys.LEFT:  # ⭠: flieg nach links
        if ufo_speed > 1:
            ufo_speed -= 1
    if key == keys.RIGHT:  # ⭢: flieg nach rechts
        if ufo_speed <= 8:
            ufo_speed += 1

    if key == keys.UP:  # ⭡: schwebe nach oben
        if horizon_y <= 420:
            ufo_lift = +3
    if key == keys.DOWN:  # : schwebe nach unten
        if horizon_y >= 360:
            ufo_lift = -3

    if star is None and key == keys.SPACE:
        star = Actor("kenney_platformer-pack-redux/png/items/star.png", (WIDTH//2, 140))
        star_descent = 0.5


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
