# Ein Ufo im Anflug auf unseren Meeresblick ...
# Copyright (C) 2021, 2023  Roland Richter             [Mu: Pygame Zero]

import random

# Fenstergröße 800 x 500
TITLE = "Rakete mit Pfeiltasten"
WIDTH = 800
HEIGHT = 500
frame_count = 0
total_time = 0

# Die Bilder stammen von https://kenney.nl/assets/alien-ufo-pack;
# alle Dateinamen wurden in kleinbuchstaben umgewandelt
ufo = Actor("kenney_alien-ufo-pack/png/shippink_manned.png", (WIDTH - 40, 140))
dome = Actor("kenney_alien-ufo-pack/png/dome.png", (WIDTH - 40, 140 - 30))

beam = Actor("kenney_alien-ufo-pack/png/laseryellow2.png")

game_over = False
speed = +3  # horizontale Geschwindigkeit: ⭠ negativ, positiv ⭢
lift = 0  # vertikale Geschwindigkeit: ⭣ negativ, positiv ⭡


def draw():
    global frame_count

    screen.fill("skyblue2")

    # Male das Meer im unteren Teil des Fensters, dahinter eine Insel
    screen.draw.filled_circle((0.5 * WIDTH, HEIGHT), 0.3 * HEIGHT, "forestgreen")
    screen.draw.filled_rect(Rect(0, 0.8 * HEIGHT, WIDTH, 0.2 * HEIGHT), "royalblue4")

    # Male eine weiße Wolke ...
    screen.draw.filled_circle((600, 150), 42, "white")
    screen.draw.filled_circle((576, 155), 42, "white")
    screen.draw.filled_circle((616, 140), 42, "white")
    screen.draw.filled_circle((632, 160), 42, "white")

    # ... und die gelbe Sonne
    screen.draw.filled_circle((150, 70), 45, "yellow")

    # Male einen Leuchtturm ...
    screen.draw.filled_rect(Rect(0.4 * WIDTH, 0.6 * HEIGHT, 20, 80), "whitesmoke")

    # ... mit einem Blinklicht, das zwischen rot und schwarz hin- und herspringt.
    light_color = "orangered" if frame_count % 120 < 20 else "black"
    screen.draw.filled_circle((0.4 * WIDTH + 10, 0.6 * HEIGHT), 10, light_color)
    # Zeichne das UFO mit der Hülle
    ufo.draw()
    dome.draw()

    # Zeige Strahl an, solange UFO hoch steigt
    if lift > 0:
        beam.midtop = (ufo.x, ufo.bottom - 30)
        beam.draw()

    if game_over:
        screen.draw.text("GAME OVER!", (200, HEIGHT / 2), color="red", fontsize=80)

    # Schreibe Informationen in die linke obere Ecke
    info = "time:" + str(round(total_time, 2))
    screen.draw.text(info, (10, 10), color="black", fontsize=24)

    # PROBIERE, weitere Informationen anzuzeigen, z.B. die Höhe des UFOs

    # Zähle die Anzahl der gemalten Fenster mit
    frame_count += 1


def update(delta):
    global speed, lift, game_over, total_time

    if game_over:
        return

    total_time += delta

    # Falls das UFO über dem Boden schwebt, dann passe seine
    # Position mit Geschwindigkeit ⭤ und Auftrieb ⭥ an ...
    if ufo.y < HEIGHT - 70:
        ufo.x += speed
        lift -= 0.2
        ufo.y -= round(lift)
    # ... sonst ist das Spiel zu Ende!
    else:
        # crash Geräusch von Gemesil, CC0
        # https://freesound.org/people/Gemesil/sounds/545692/
        sounds.gemesil_crash.play()
        # PROBIERE das Bild des UFOs so zu ändern, dass es beschädigt aussieht
        game_over = True

    # Falls rechter bzw. linker Rand erreicht, beginne am linken bzw rechten Rand
    if speed > 0 and ufo.x > WIDTH:
        ufo.x = 0
    if speed < 0 and ufo.x < 0:
        ufo.x = WIDTH

    # # Lass das Ufo wackeln; erzeuge dazu einen zufälligen Winkel ...
    if not game_over:
        winkel = random.randint(-8, 8)
        ufo.angle = winkel
    else:
        ufo.angle = -45

    # # ... und eine zufällige vertikale Bewegung des UFOs
    auf_ab = random.randint(-2, 2)
    ufo.y += auf_ab

    # Die Hülle des UFOs muss ebenfalls mitbewegt werden
    dome.x = ufo.x
    dome.y = ufo.y - 30
    dome.angle = ufo.angle


# Verändere Geschwindigkeit und Auftrieb mit ⭠, ⭢ und ⭡; siehe auch
# https://pygame-zero.readthedocs.io/en/stable/hooks.html#buttons-and-keys
def on_key_down(key):
    global speed, lift, total_time

    if key == keys.LEFT:  # ⭠: flieg nach links
        speed += -3
    if key == keys.RIGHT:  # ⭢: flieg nach rechts
        speed += +3
    if key == keys.UP:  # ⭡: schwebe nach oben
        lift = +6
    # Starte Spiel mit Leertaste neu: das UFO wird
    # in den Himmel teleportiert, Geschwindigkeiten auf 0 gesetzt
    global game_over
    if game_over and key == keys.SPACE:
        ufo.y = 0
        speed = lift = 0
        total_time = 0
        game_over = False


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
