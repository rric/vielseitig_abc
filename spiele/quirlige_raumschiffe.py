# Ein Ufo im Anflug auf unseren Meeresblick ...
# Copyright 2021, 2023  Roland Richter                 [Mu: Pygame Zero]

import random

# Fenstergröße 800 x 500
TITLE = "Quirlige Raumschiffe"
WIDTH = 800
HEIGHT = 500
frame_count = 0

# Die Bilder stammen von https://kenney.nl/assets/alien-ufo-pack;
# alle Dateinamen wurden mit dem Befehl
# for /f "Tokens=*" %f in ('dir /l/b/a-d') do (rename "%f" "%f")
# in kleinbuchstaben umgewandelt
ufo = Actor("kenney_alien-ufo-pack/png/shippink_manned.png", (WIDTH - 40, 140))
dome = Actor("kenney_alien-ufo-pack/png/dome.png", (WIDTH - 40, 140 - 30))

# PROBIERE ein UFO in einer anderen Farbe aus; oder probiere, das Bild
# des UFOs selbst zu zeichnen.

def draw():
    global frame_count
    frame_count += 1

    screen.fill("skyblue2")

    # Male das Meer im unteren Teil des Fensters, dahinter eine Insel
    screen.draw.filled_circle((0.5 * WIDTH, HEIGHT), 0.3 * HEIGHT, "forestgreen")
    screen.draw.filled_rect(Rect(0, 0.8 * HEIGHT, WIDTH, 0.2 * HEIGHT), "royalblue4")

    # Male eine weiße Wolke ...
    screen.draw.filled_circle((600, 150), 42, "white")
    screen.draw.filled_circle((576, 155), 42, "white")
    screen.draw.filled_circle((616, 140), 42, "white")
    screen.draw.filled_circle((632, 160), 42, "white")

    # ... und die gelbe Sonne
    screen.draw.filled_circle((150, 70), 45, "yellow")

    # Male einen Leuchtturm ...
    screen.draw.filled_rect(Rect(0.4 * WIDTH, 0.6 * HEIGHT, 20, 80), "whitesmoke")

    # ... mit einem Blinklicht, das zwischen rot und schwarz hin- und herspringt.
    light_color = "red" if frame_count % 120 < 20 else "black"
    screen.draw.filled_circle((0.4 * WIDTH + 10, 0.6 * HEIGHT), 10, light_color)

    # Zeichne das UFO mit der Hülle
    ufo.draw()
    dome.draw()

    # Schreibe Informationen in die linke obere Ecke
    info = "frame " + str(frame_count)
    screen.draw.text(info, (10, 10), color="white", fontsize=24)


def update():
    # Bewege das UFO nach rechts; ...
    ufo.x += 2
    # falls rechter Rand erreicht, beginne wieder am linken Rand.
    if ufo.x > WIDTH:
        ufo.x = 0

    # PROBIERE Lass das Ufo mit Hilfe von Zufallszahlen wackeln:
    auf_ab = random.randint(-8, 8)
    # QUIZ Jetzt muss 'auf_ab' die Bewegung des UFOs beeinflussen; wie?

    # QUIZ Wie kannst du hier einen zufälligen Winkel erzeugen?
    winkel = 0
    ufo.angle = winkel

    # Die Hülle des UFOs muss ebenfalls mitbewegt werden
    dome.x = ufo.x
    dome.y = ufo.y - 30
    dome.angle = ufo.angle


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
